# VimeStats
Discord бот для проверки статистики с VimeWorld

## Команды бота
- `help` | Помощь по командам
- `ping` | Проверить пинг клиента
- `staff` | Онлайн модераторов
- `stats <никнейм>` | Статистика пользователя
- `guild <id/tag/name> <запрос>` | Статистика гильдии
- `streams` | Стримы на сервере

## Небольшое уведомление
Дизайн команд (`staff`, `stats`, `guild`, `streams`) похож на тот, что у бота от VimeTop. Я пытаюсь скопировать его, и добавить в него новые функции. А также я даю Open Source, можете добавлять новые функции в бота через Pull Request. :D

## Библиотеки которые использовались при создании бота
- [`discore.js`](https://github.com/zargovv/discore.js) (Based on [DiscordJS](https://github.com/discordjs/discord.js))
- `moment`
- `moment-duration-format`
- `request-promise-native`
